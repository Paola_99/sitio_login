Este es el README de un un login y en base al usuario muestre los roles asignados.
Los pasos que se deben seguir, para el correcto funcionamiento del login serán los siguientes:
1er Paso:
 Instalar las siguientes programas: Xampp, MySQL-Front y un editor de codigo por ejemplo Sublime Text
2do Paso: 
 1. Descargar el proyecto de GitLab
 2. Mover el proyecto a la siguiente dirección --> C:\xampp\htdocs
 3. Ingresar a Xamp e inicializar los servicios de apache y MySql 
 NOTA: en caso de error al inicalizar apache, seguir los siguientes pasos:
	- Hacer Click en el boton "Config" de Apache
	- Ingresar a Apache(httpd.conf)
	- Modificar localhost:80 a Localhost:8080 y el #Listen 12.34.56.78:80 a #Listen 12.34.56.78:8080
 4. Abrir una conexion en MySQL-Front, ingresando los datos de Nombre: root, Conexion Host: localhost 
 5. Importa la base de datos a MySQL-Front que se encuentra dentro del proyecto con el nombre de ejemplos.sql
 6. Ingresar a un navegador web
 7. Ingresar la siguiente direccion http://localhost:80/login

NOTA: Los usuarios a probar en el proyecto son los siguientes:

	USUARIOS		CONTRASEÑA		ROL
	Uprueba 		contrasenia  		admin
	User1  			abc123       		invitado
	User2  			abc1234      		usuario
