<!DOCTYPE html> 
<html> 
  <head>
    <meta charset="utf-8"> 
    <title>Login de usuarios</title> 
	  <link rel="stylesheet" href="css/normalize.min.css">
    <link href="css/bootstrap.css" rel="stylesheet"> 
  </head> 
  <body> 
    <div class="container">
      <div class="row">
        <div class="col-md-5 col-md-offset-3">
          <div class="panel panel-primary">
            <div class="panel-heading">
              INICIAR SESION
            </div>
            <div class="panel-body">
              <form class="form-horizontal" role="form" action="login.php" method="post">
                <div class="form-group">
                  <label for="usuario" class="col-sm-3 control-label">Nombre de usuario:</label>
                  <div class="col-sm-9">
                    <input type="text" class="form-control" id="usuario" name="login">
                  </div>
                </div>
                <div class="form-group">
                  <label for="clave" class="col-sm-3 control-label">Contraseña:</label>
                    <div class="col-sm-9">
                      <input type="password" class="form-control" id="clave" name="password">
                    </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-default">Login</button>
                  </div> 
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body> 
</html> 