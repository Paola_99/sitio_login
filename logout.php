<?php
ob_start();
?>
<?php
session_cache_limiter(FALSE);
session_start();
if(isset($_SESSION['login']))
{
	session_destroy();
	header("Location: index.php");
}
else
{
	header("Location: index.php");
}
?>
<?php
ob_end_flush();
?>