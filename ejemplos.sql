﻿# Host: localhost  (Version 5.5.5-10.1.19-MariaDB)
# Date: 2020-03-12 14:19:00
# Generator: MySQL-Front 6.0  (Build 2.20)


#
# Structure for table "usuarios"
#

DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` tinyint(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(100) NOT NULL,
  `rol` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

#
# Data for table "usuarios"
#

/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Uprueba','c55d52d0b68a73e44dd7dc6bd0381b47','admin'),(4,'User1','e99a18c428cb38d5f260853678922e03','invitado'),(10,'User2','a141c47927929bc2d1fb6d336a256df4','Usuario'),(11,'User','c4ca4238a0b923820dcc509a6f75849b','usuario'),(12,'User','c4ca4238a0b923820dcc509a6f75849b','usuario');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
